﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace csharp3_lesson11
{
    public class RockGame
    {
        private Counter<Igenous> _myIgneous = new Counter<Igenous>();
        private Counter<Sedimentary> _mySedimentary = new Counter<Sedimentary>();
        private Counter<Metamorphic> _myMetamorphic = new Counter<Metamorphic>();


        public int CountIgneous { get { return _myIgneous.Count; } }
        public int CountSedimentary { get { return _mySedimentary.Count; } }
        public int CountMetamorphic { get { return _myMetamorphic.Count; } }

        // Private class variables
        private Counter<Rock> _myCounter = new Counter<Rock>();
        private int _coinHeight = 20;
        private Random _random = new Random();

        // Private backing class variables
        private decimal _targetAmount = 0.00m;

        // Accessors
        public decimal TargetAmount
        {
            set { _targetAmount = value; }
            get { return _targetAmount; }
        }

        // Constructors
        public RockGame()
        {
            SetTargetAmount();
        }

        //  Public methods
        public void Add(Rock rock)
        {
            _myCounter.Add(rock);
            switch (rock.GetType().Name)
            {
                case "Igenous":
                    _myIgneous.Add(rock as Igenous);
                    break;
                case "Sedimentary":
                    _mySedimentary.Add(rock as Sedimentary);
                    break;
                case "Metamorphic":
                    _myMetamorphic.Add(rock as Metamorphic);
                    break;
            }

        }

        public void Update(Graphics graphics, Size boardSize)
        {
            // Note using for loop to allow updating rock
            for (int i = 0; i < _myCounter.Count; i++)
            {
                Rock rock = _myCounter[i];
                // Set location
                int startingY = boardSize.Height - (i * (_coinHeight + 1)) - _coinHeight - 1;
                rock.Location = new Point(1, startingY);
                rock.Dimensions = new Size(boardSize.Width - 2, _coinHeight);
                // Draw coin
                using (SolidBrush solidBrush = new SolidBrush(rock.Color))
                    graphics.FillRectangle(solidBrush, new Rectangle(rock.Location, rock.Dimensions));
                // Draw text
                RectangleF boundingRectangle = new RectangleF(rock.Location.X, rock.Location.Y, rock.Dimensions.Width, rock.Dimensions.Height);
                using (Font font = new Font("Arial", 12, FontStyle.Bold))
                using (StringFormat stringFormat = new StringFormat())
                using (SolidBrush brush = new SolidBrush(Color.White))
                {
                    // Align text horizontally and vertically
                    stringFormat.Alignment = StringAlignment.Center;
                    stringFormat.LineAlignment = StringAlignment.Center;
                    graphics.DrawString(rock.Text, font, brush, boundingRectangle, stringFormat);
                }
            }
        }

        public void Remove(Point location)
        {
            // Loop through each coin to see which was selected
            foreach (Rock rock in _myCounter)
            {
                if (rock.Hit(location))
                {
                    switch (rock.GetType().Name)
                    {
                        case "Igenous":
                            _myIgneous.Remove(rock as Igenous);
                            break;
                        case "Sedimentary":
                            _mySedimentary.Remove(rock as Sedimentary);
                            break;
                        case "Metamorphic":
                            _myMetamorphic.Remove(rock as Metamorphic);
                            break;
                    }
                    _myCounter.Remove(rock);
                    break;
                }
            }
    

        }

        public void Reset()
        {
            _myCounter.Reset();
            _myIgneous.Reset();
            _mySedimentary.Reset();
            _myMetamorphic.Reset();
            SetTargetAmount();
        }

        // Private methods
        private void SetTargetAmount()
        {
            _targetAmount = _random.Next(1, 100) / 100.00m;
        }
    }
}

