﻿namespace csharp3_lesson11
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addIgneousButton = new System.Windows.Forms.Button();
            this.addSedimentaryButton = new System.Windows.Forms.Button();
            this.addMetamorphicButton = new System.Windows.Forms.Button();
            this.resetButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.moneyPictureBox = new System.Windows.Forms.PictureBox();
            this.currentIgneousCountLabel = new System.Windows.Forms.Label();
            this.currentSedimentaryCountLabel = new System.Windows.Forms.Label();
            this.currentMetamorphicCountLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.moneyPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // addIgneousButton
            // 
            this.addIgneousButton.Location = new System.Drawing.Point(49, 46);
            this.addIgneousButton.Name = "addIgneousButton";
            this.addIgneousButton.Size = new System.Drawing.Size(130, 41);
            this.addIgneousButton.TabIndex = 0;
            this.addIgneousButton.Text = "Add Igneous Rock";
            this.addIgneousButton.UseVisualStyleBackColor = true;
            // 
            // addSedimentaryButton
            // 
            this.addSedimentaryButton.Location = new System.Drawing.Point(49, 105);
            this.addSedimentaryButton.Name = "addSedimentaryButton";
            this.addSedimentaryButton.Size = new System.Drawing.Size(130, 45);
            this.addSedimentaryButton.TabIndex = 1;
            this.addSedimentaryButton.Text = "Add Sedimentary Rock";
            this.addSedimentaryButton.UseVisualStyleBackColor = true;
            // 
            // addMetamorphicButton
            // 
            this.addMetamorphicButton.Location = new System.Drawing.Point(49, 185);
            this.addMetamorphicButton.Name = "addMetamorphicButton";
            this.addMetamorphicButton.Size = new System.Drawing.Size(130, 42);
            this.addMetamorphicButton.TabIndex = 2;
            this.addMetamorphicButton.Text = "Add Metamorphic Rock";
            this.addMetamorphicButton.UseVisualStyleBackColor = true;
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(49, 398);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(75, 23);
            this.resetButton.TabIndex = 4;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(49, 446);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 5;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(311, 446);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(130, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Click rock in list to remove";
            // 
            // moneyPictureBox
            // 
            this.moneyPictureBox.Location = new System.Drawing.Point(279, 55);
            this.moneyPictureBox.Name = "moneyPictureBox";
            this.moneyPictureBox.Size = new System.Drawing.Size(175, 366);
            this.moneyPictureBox.TabIndex = 14;
            this.moneyPictureBox.TabStop = false;
            // 
            // currentIgneousCountLabel
            // 
            this.currentIgneousCountLabel.AutoSize = true;
            this.currentIgneousCountLabel.Location = new System.Drawing.Point(205, 55);
            this.currentIgneousCountLabel.Name = "currentIgneousCountLabel";
            this.currentIgneousCountLabel.Size = new System.Drawing.Size(13, 13);
            this.currentIgneousCountLabel.TabIndex = 15;
            this.currentIgneousCountLabel.Text = "0";
            // 
            // currentSedimentaryCountLabel
            // 
            this.currentSedimentaryCountLabel.AutoSize = true;
            this.currentSedimentaryCountLabel.Location = new System.Drawing.Point(208, 119);
            this.currentSedimentaryCountLabel.Name = "currentSedimentaryCountLabel";
            this.currentSedimentaryCountLabel.Size = new System.Drawing.Size(13, 13);
            this.currentSedimentaryCountLabel.TabIndex = 16;
            this.currentSedimentaryCountLabel.Text = "0";
            // 
            // currentMetamorphicCountLabel
            // 
            this.currentMetamorphicCountLabel.AutoSize = true;
            this.currentMetamorphicCountLabel.Location = new System.Drawing.Point(208, 195);
            this.currentMetamorphicCountLabel.Name = "currentMetamorphicCountLabel";
            this.currentMetamorphicCountLabel.Size = new System.Drawing.Size(13, 13);
            this.currentMetamorphicCountLabel.TabIndex = 17;
            this.currentMetamorphicCountLabel.Text = "0";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 488);
            this.Controls.Add(this.currentMetamorphicCountLabel);
            this.Controls.Add(this.currentSedimentaryCountLabel);
            this.Controls.Add(this.currentIgneousCountLabel);
            this.Controls.Add(this.moneyPictureBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.addMetamorphicButton);
            this.Controls.Add(this.addSedimentaryButton);
            this.Controls.Add(this.addIgneousButton);
            this.Name = "MainForm";
            this.Text = "Coins";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.moneyPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addIgneousButton;
        private System.Windows.Forms.Button addSedimentaryButton;
        private System.Windows.Forms.Button addMetamorphicButton;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox moneyPictureBox;
        private System.Windows.Forms.Label currentIgneousCountLabel;
        private System.Windows.Forms.Label currentSedimentaryCountLabel;
        private System.Windows.Forms.Label currentMetamorphicCountLabel;
    }
}

