﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp3_lesson11
{
    public partial class MainForm : Form
    {
        private RockGame _rockGame = new RockGame();

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            // Exit event lambda expression
            exitButton.Click += (from, ea) => this.Close();

            // Reset event lambda expression
            resetButton.Click += (from, ea) =>
            {
                _rockGame.Reset();
                moneyPictureBox.Invalidate();
      
            };

            // PictureBox Mouseup lambda expression
            addIgneousButton.Click += (from, ea) => { _rockGame.Add(new Igenous()); moneyPictureBox.Invalidate();  };
            
            addSedimentaryButton.Click += (from, ea) => { _rockGame.Add(new Sedimentary()); moneyPictureBox.Invalidate();  };
            addMetamorphicButton.Click += (from, ea) => { _rockGame.Add(new Metamorphic()); moneyPictureBox.Invalidate();  };
          

            // Setup Picturebox Paint event
            moneyPictureBox.Paint += (from, ea) =>
            {
                _rockGame.Update(ea.Graphics, moneyPictureBox.ClientSize);
                currentIgneousCountLabel.Text = _rockGame.CountIgneous.ToString();
                currentSedimentaryCountLabel.Text = _rockGame.CountSedimentary.ToString();
                currentMetamorphicCountLabel.Text = _rockGame.CountMetamorphic.ToString();


            };

            // PictureBox Mouseup lambda expression
            moneyPictureBox.MouseUp += (from, ea) => { _rockGame.Remove(ea.Location); moneyPictureBox.Invalidate(); };

        }




    }
}
