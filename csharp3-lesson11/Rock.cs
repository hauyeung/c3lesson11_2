﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace csharp3_lesson11
{
    public class Rock
    {
        

        // Private backing class variables
        private decimal _worth;
        private Point _location;
        private Size _dimensions;
        private string _text;
        private Color _color;

        // Accessors
        public decimal Worth { get { return _worth; } }
        public Point Location { get { return _location; } set { _location = value; } }
        public Size Dimensions { get { return _dimensions; } set { _dimensions = value; } }
        public string Text { get { return _text; } set { _text = value; } }
        public Color Color { get { return _color; } set { _color = value; } }
        public int count { get; set; }

        // Constructors
        public Rock(decimal worth, string text, Color color)
        {
            _worth = worth;
            _text = text;
            _color = color;
        }

        // Public methods
        public bool Hit(Point location)
        {
            return new Rectangle(_location, _dimensions).Contains(location);
        }
    }
    public class Igenous : Rock
    {
        public Igenous() : base(0.01m, "Igenous", Color.Brown) { }
    }

    public class Sedimentary : Rock
    {
        public Sedimentary() : base(0.01m, "Sedimentary", Color.Red) { }
    }

    public class Metamorphic : Rock
    {
        public Metamorphic() : base(0.05m, "Metamorphic", Color.Silver) { }
    }


}
